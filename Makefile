all: main.c
	gcc -Wall -g main.c -o run \
		`pkg-config --libs gtk+-2.0` \
		`pkg-config --cflags gio-unix-2.0`

clean:
	$(RM) run

