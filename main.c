#include <stdio.h>
#include <glib.h>
#include <gio/gio.h>
#include <gio/gdesktopappinfo.h>

int main(int argc, char** argv) {

	if (argc == 1) {
		printf("Usage: %s <desktop ids>\n", argv[0]);
		return 64;
	}

	for (int i = 1; i < argc; i++) {
		char* app_id = argv[i];

		if (!g_str_has_suffix(app_id, ".desktop")) {
			app_id = malloc(strlen(app_id) + 8);
			strcat(app_id, argv[i]);
			strcat(app_id, ".desktop");
		}

		printf("Launching: '%s'\n", app_id);

		GAppInfo* app = (GAppInfo*) g_desktop_app_info_new(app_id);
		GError* err = NULL;

		if (app == NULL) {
			printf("Error Launching: '%s' Not Found\n", app_id);
			continue;
		}

		gboolean success = g_app_info_launch(app,
			NULL,
			NULL,
			&err);

		if (!success) {
			printf("Error launching: '%s' %s\n",
				g_app_info_get_display_name(app),
				err->message);

			g_error_free(err);
		}
	}

	return 0;
}

